﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] List<GameObject> clouds;
    [SerializeField] List<GameObject> units;
    [SerializeField] int level;
    GameObject levelPref;
    UiController ui;
    Canvas canvas;

    private void Awake()
    {
        ui = FindObjectOfType<UiController>();
        canvas = FindObjectOfType<Canvas>();
        levelPref = Resources.Load("");
    }

    private void Start()
    {
        StartCoroutine(MoveClouds());
        ui.ShowLevel(level);
    }

    IEnumerator MoveClouds()
    {
        for (int i = 0; i < clouds.Count; i++)
        {
            StartCoroutine(clouds[i].GetComponent<Cloud>().Move());
            yield return new WaitForSeconds(10f);
        }
    }

    void ShowCastle()
    {
        
    }

}
