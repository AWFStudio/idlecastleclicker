﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UiController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI coinsText;
    [SerializeField] TextMeshProUGUI levelText;
    [SerializeField] GameObject marketPanel;
    int coins;

    private void Start()
    {
        ShowCoins();
    }

    public void AddCoins(int amount)
    {
        coins += amount;
        ShowCoins();
    }

    void ShowCoins()
    {
        coinsText.text = coins.ToString();
    }

    public void ShowLevel(int level)
    {
        levelText.text = "Level " + level.ToString();
    }
}
