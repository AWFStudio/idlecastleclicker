﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectProfitBttn : MonoBehaviour
{
    UnitController unit;
    UiController ui;

    private void Awake()
    {
        unit = GetComponentInParent<UnitController>();
        ui = FindObjectOfType<UiController>();
    }

    public void Press()
    {
        ui.AddCoins(unit.CurrentPrifit);
        unit.StartTimer();
        gameObject.SetActive(false);
    }
}
