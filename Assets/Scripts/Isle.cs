﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Isle : MonoBehaviour
{
    [SerializeField] GameObject maxHeight;
    [SerializeField] GameObject minHeight;

    Vector3 pos;


    float speed = 0.1f;

    private void Start()
    {
        pos = maxHeight.transform.position;
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        while (Vector3.Distance(transform.position, pos) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * speed);
            yield return null;
        }
        transform.position = pos;
        if (pos == maxHeight.transform.position)
        {
            pos = minHeight.transform.position;
        }
        else
        {
            pos = maxHeight.transform.position;
        }
        StartCoroutine(Move());
    }
}
