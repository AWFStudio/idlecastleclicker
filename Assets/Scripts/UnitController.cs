﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitController : MonoBehaviour
{
    [SerializeField] int unitID;
    [SerializeField] GameObject collectProfitBttn;
    [SerializeField] Slider timerSlider;

    [Header("Debug")]
    [SerializeField] string unitName;
    [SerializeField] int baseLevel;
    [SerializeField] int currentLevel;
    [SerializeField] int maxLevel;
    [SerializeField] float baseInterval;
    [SerializeField] float currentInterval;
    [SerializeField] float intervalDecrease;
    [SerializeField] int basePrifit;
    [SerializeField] int currentPrifit;
    [SerializeField] int profitIncrease;
    [SerializeField] int cost;
    [SerializeField] int levelingCost;

    public int CurrentPrifit { get => currentPrifit; set => currentPrifit = value; }

    private void Start()
    {
        SetValues();
        StartTimer();
    }

    void SetValues()
    {
        var sheet = new ES3Spreadsheet();
        sheet.Load("units.csv");

        unitName = sheet.GetCell<string>(0, unitID);
        baseLevel = currentLevel = sheet.GetCell<int>(1, unitID);
        maxLevel = sheet.GetCell<int>(2, unitID);
        baseInterval = currentInterval = sheet.GetCell<int>(3, unitID);
        basePrifit = CurrentPrifit = sheet.GetCell<int>(4, unitID);
        profitIncrease = sheet.GetCell<int>(5, unitID);
        intervalDecrease = sheet.GetCell<int>(6, unitID);
        cost = sheet.GetCell<int>(7, unitID);
        levelingCost = sheet.GetCell<int>(8, unitID);
    }

    public void StartTimer()
    {
        float time = 0f;
        timerSlider.maxValue = currentInterval;
        timerSlider.value = time;
        StartCoroutine(ProfitTimer(time));
    }

    IEnumerator ProfitTimer(float time)
    {
        while (time < currentInterval)
        {
            time += Time.deltaTime;
            timerSlider.value = time;
            yield return Time.deltaTime;
        }
        collectProfitBttn.SetActive(true);
    }


}
