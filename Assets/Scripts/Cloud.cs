﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cloud : MonoBehaviour
{
    [SerializeField] List<Sprite> clouds;
    [SerializeField] GameObject startPos;
    [SerializeField] GameObject endPos;

    float speed = 0.3f;

    public IEnumerator Move()
    {
        int randomSprite = Random.Range(0, clouds.Count);
        GetComponent<Image>().sprite = clouds[randomSprite];
        GetComponent<Image>().SetNativeSize();
        float randomHeight = Random.Range(startPos.transform.position.y, endPos.transform.position.y);
        transform.position = new Vector3(startPos.transform.position.x, randomHeight);

        while (transform.position.x > endPos.transform.position.x)
        {
            transform.Translate(Vector3.left * Time.deltaTime * speed);
            yield return null;
        }
        StartCoroutine(Move());
    }
}
