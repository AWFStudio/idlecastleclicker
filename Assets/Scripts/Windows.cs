﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Windows : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(Blink());
    }

    IEnumerator Blink()
    {
        int randomWindow = Random.Range(0, transform.childCount);
        GameObject temp = transform.GetChild(randomWindow).gameObject;
        temp.SetActive(true);
        yield return new WaitForSeconds(5f);
        temp.SetActive(false);
        StartCoroutine(Blink());
    }


}
